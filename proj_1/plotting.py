from matplotlib import pyplot as plt
names = [
    "Flan_1565",
    "StocF-1465",
    "cfd2",
    "cfd1",
    "G3_circuit",
    "parabolic_fem",
    "apache2",
    "shallow_water1",
    "ex15"
]

error = [
    1.4805e-12,
    0.00000000024229,
    2.8743e-13,
    2.7668e-14,
    4.9097e-12,
    8.3753e-13,
    0.000000000050085,
    2.0501e-16,
    0.00000060515
]

elapsed = [
    531.12,
    695.19,
    3.0257,
    1.3885,
    14.387,
    3.4179,
    9.7523,
    0.41387,
    0.030326
]

plt.figure()
plt.title("Errore")
plt.xlabel("Matrici")
plt.ylabel("Errore")
plt.bar(names,error)
plt.yscale("log")
plt.show()

plt.figure()
plt.title("Tempistiche")
plt.xlabel("Matrici")
plt.ylabel("Secondi")
plt.bar(names,elapsed)
plt.yscale("log")
plt.show()