from scipy.fftpack import dct,idct
from myFunctions import myDCT
import matplotlib.pyplot as plt
import numpy
import time
import math

min_dim = 8 # Dimensione di partenza
max_dim = 500 # Dimensione massima finale
step = 25

mylib_time = []
official_time = []
dimensioni = []

trend_cubico = []
trend_logaritmico = []

for n in range(min_dim,max_dim,step):

    print("passo: {0}".format(n))
    
    dimensioni.append(n)

    # Matrice random min 0 max 255
    originale = numpy.random.randint(256,size=(n,n))

    start_time = time.time()

    # Applico la dct su righe e colonne per fare la DCT2 (libreria)
    matrice_dct = dct(dct(originale,norm="ortho").T,norm="ortho").T

    #print("algoritmo originale ci mette: {0}".format(time.time() - start_time))
    elapsed = time.time() - start_time

    if elapsed == 0:
        elapsed = 0.001

    print("ufficiale: {0}".format(elapsed))

    official_time.append(elapsed)

    # Creo una matrice di zeri
    matrice_mydct = numpy.zeros((n,n))

    start_time = time.time()

    # Applico la myDCT sulle righe
    for row in range(len(originale)):
        matrice_mydct[row] = myDCT(originale[row])

    # Faccio la trasposta in modo da poter applicare la mydct sulle colonne
    matrice_mydct = matrice_mydct.transpose()

    # Applico la myDCT sulle colonne
    for col in range(len(originale)):
        matrice_mydct[col] = myDCT(matrice_mydct[col])

    # Faccio la trasposta di nuovo per portarla nella forma originale
    matrice_mydct = matrice_mydct.transpose()

    elapsed = time.time() - start_time

    mylib_time.append(elapsed)
    print("personale: {0}".format(elapsed))

    # Calcolo del trend per confronto
    trend_cubico.append(n ** 3)
    trend_logaritmico.append((n ** 2) * math.log(n))

# Plot del grafico
plt.figure()
plt.title("Tempistiche algoritmi")
plt.xlabel("Dimensione")
plt.ylabel("Tempo")
plt.plot(dimensioni,official_time,label="libreria DCT",marker=".")
plt.plot(dimensioni,mylib_time,label="DCT personale",marker=".")
plt.plot(dimensioni,trend_cubico,label="trend N^3",marker=".")
plt.plot(dimensioni,trend_logaritmico,label="trend N^2log(N)",marker=".")
plt.yscale("log")
plt.legend(loc="upper right")
plt.show()
