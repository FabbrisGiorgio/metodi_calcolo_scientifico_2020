import math

def myDCT(vett):
    """
    Compute the DCT of the array given in input.

    Arguments:
    double vett[] = array on which the DCT will be calculated
    """

    N = len(vett)
    result = []

    for k in range(N):
        somma = 0

        # Valore da moltiplicare alla sommatoria
        if k == 0:
            alpha = math.sqrt(1/N)
        else:
            alpha = math.sqrt(2/N) # 1/(N/2)
        
        # Sommatoria (vedi formula)
        for i in range(N):
            somma += vett[i] * math.cos(k * math.pi * ((2 * i + 1)/(2 * N)))
        
        result.append(alpha*somma)
    
    return result
