baseFileNames = ["ex15.mat", "cfd1.mat"];
% , "shallow_water1.mat", "cfd2.mat",
% "parabolic_fem.mat", "apache2.mat", "G3_circuit.mat",           "StocF-1465.mat", "Flan_1565.mat"
folder = '/home/lorenzo/Documenti';
matrixSizes = zeros(9, 1);
memoryUsages = zeros(9, 1);
relativeErrors = zeros(9, 1);
elapsedTimes = zeros(9, 1);
i = 1;

for fileName = baseFileNames
    fullFileName = fullfile(folder, fileName);
    if exist(fullFileName, 'file')
        % Caricamento del file
        loadedFile = load(fullFileName);
        matrix = loadedFile.Problem.A;
        matrixSize = size(matrix, 1);
        
        matrix_chol = chol(matrix);
        
        % Creazione del vettore di soluzione esatta
        xe = ones(1, matrixSize);
        xe = transpose(xe);
        % Calcolo del coefficiente noto
        b = matrix * xe;
        b_chol = matrix_chol * xe;
        
        % Utilizzo della memoria a inizio elaborazione
%         user1 = memory;
        
        % Calcolo del tempo di risoluzione del sistema lineare
        tic
        x = matrix\b;
        elapsedTime = toc;
        
        tic
        x_chol = matrix_chol\b_chol;
        elapsedTime_chol = toc;
        
        % Utilizzo della memoria a fine elaborazione
%         user2 = memory;
        
%         memoryUsed = user2.MemUsedMATLAB - user1.MemUsedMATLAB;
        
        relativeError = norm(x - xe) / norm(xe);

        relativeError_chol = norm(x_chol - xe) / norm(xe);
        
        matrixSizes(i) = matrixSize;
%         memoryUsages(i) = memoryUsed;
        relativeErrors(i) = relativeError;
        elapsedTimes(i) = elapsedTime;
    end
    i = i + 1;
end
    
    semilogy(matrixSizes, elapsedTimes)
    hold on
%     semilogy(matrixSizes, memoryUsages)
    semilogy(matrixSizes, relativeErrors)
    hold off
    xlabel('Matrix size')
    ylabel('Time and relative error')