clc;
clear;

matrixData = importdata('matrici/Flan_1565.mat');

matrix = matrixData.A;

dim = size(matrix, 1);
xe = ones(dim, 1);
b = matrix*xe;

tic

x = matrix \ b;

elapsed = toc;

error = norm(x - xe) / norm(xe);