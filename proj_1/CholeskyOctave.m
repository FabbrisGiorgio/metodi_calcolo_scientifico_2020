clc;

% Import matrice
matrix_data = load("ex15.mat");

% Estrazione matrice dal file
matrix = matrix_data.Problem.A;

% Ottengo la dimensione
dim = size(matrix,1);

% Creo il vettore soluzione e termine noto b
xe = ones(dim,1);
b = matrix * xe;

tic ();

% Calcolo soluzione
x = matrix \ b;

elapsed = toc ();

% Calcolo errore relativo
error = norm( x - xe) / norm(xe);
