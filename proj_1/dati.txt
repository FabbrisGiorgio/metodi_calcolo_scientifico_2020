LINUX (memory in kb)

MATLAB
Flan_1565	error:  elapsed: 
		memoryBefore: memoryAfter:
StocF-1465	error:  elapsed: 
		memoryBefore: memoryAfter: 
cfd2 		error: 4.189323495271399e-13	elapsed: 2.350105000000000
		memoryBefore: 1001475		memoryAfter: 1740831
cfd1 		error: 1.006738180099319e-13	elapsed: 1.068347000000000
		memoryBefore: 1003315		memoryAfter: 1377520
G3_circuit 	error: 3.575744823750031e-12	elapsed: 8.819497000000000
		memoryBefore: 1024100		memoryAfter: 2764802
parabolic_fem 	error: 1.047873422496352e-12	elapsed: 1.905936000000000
		memoryBefore: 1023598		memoryAfter: 1545110
apache2 	error: 4.391378345849477e-11 	elapsed: 6.039794000000000
		memoryBefore: 1034240		memoryAfter: 2560172
shallow_water1	error: 2.377267481913067e-16 	elapsed: 0.167246000000000
		memoryBefore: 885657		memoryAfter: 887091
ex15 		error: 6.211011770321590e-07 	elapsed: 0.009853000000000
		memoryBefore: 792780		memoryAfter: 793804

OCTAVE
Flan_1565	error: elapsed:
		memBefore: memAfter: 
StocF-1465	error: elapsed:
		memBefore: memAfter:
cfd2 		error: 3.155184347682741e-13 	elapsed: 3.875621110128766
		memBefore: 193123		memAfter: 1179886
cfd1 		error: 2.553323435220982e-14 	elapsed: 1.651065111160278
		memBefore: 189789		memAfter: 518104
G3_circuit 	error: 4.909473819732179e-12 	elapsed: 17.57146820538703
		memBefore: 181239		memAfter: 2908278
parabolic_fem 	error: 8.370911920927626e-13 	elapsed: 4.584723809205485
		memBefore: 206671		memAfter: 760391
apache2 	error: 5.008276218294963e-11 	elapsed: 12.103549390168043
		memBefore: 201722		memAfter: 2171043
shallow_water1 	error: 2.050054927115491e-16 	elapsed: 0.698480811687485
		memBefore: 123197		memAfter: 188734
ex15 		error: 6.029412006080359e-07 	elapsed: 0.061545221466064453
		memBefore: 122880		memAfter: 122998

WINDOWS (memory in kb)

MATLAB
Flan_1565	error: elapsed:
		memBefore: memAfter: 
StocF-1465	error: elapsed: 
		memBefore: memAfter: 
cfd2 		error: 3.966240636620346e-13 	elapsed: 1.667477400000000
		memBefore: 1058580		memAfter: 1856369
cfd1 		error: 1.094133321260394e-13 	elapsed: 0.846664500000000
		memBefore: 1046232		memAfter: 1457504
G3_circuit 	error: 3.574849075166250e-12 	elapsed: 5.970103200000000
		memBefore: 1187260		memAfter: 1545172
parabolic_fem 	error: 1.047873422496352e-12 	elapsed: 1.804175500000000
		memBefore: 1101936		memAfter: 1196184
apache2 	error: 4.391378345849477e-11 	elapsed: 5.829686000000000
		memBefore: 1098520		memAfter: 3061268
shallow_water1 	error: 2.377267481913067e-16 	elapsed: 0.191057900000000
		memBefore: 998376		memAfter: 998692
ex15 		error: 6.211011770321590e-07 	elapsed: 0.012458800000000
		memBefore: 985384		memAfter: 989136

OCTAVE
Flan_1565	error: elapsed:
		memBefore: memAfter: 
StocF-1465	error: elapsed:
		memBefore: memAfter:
cfd2 		error: 3.155184540082741e-13 	elapsed: 3.335007905960083
		memBefore: 263144		memAfter: 1239696
cfd1 		error: 2.553326735220982e-14 	elapsed: 1.651065111160278
		memBefore: 239912		memAfter: 691214
G3_circuit 	error: 4.909473819732179e-12 	elapsed: 20.62915420532227
		memBefore: 380264		memAfter: 2808548
parabolic_fem 	error: 8.370987920927926e-13 	elapsed: 4.754388809204102
		memBefore: 306668		memAfter: 880181
apache2 	error: 5.008276267594963e-11 	elapsed: 13.28749394416809
		memBefore: 307252		memAfter: 2371096
shallow_water1 	error: 2.050054927115491e-16 	elapsed: 0.7228629589080811
		memBefore: 216264		memAfter: 275428
ex15 		error: 6.029412006080359e-07 	elapsed: 0.05763721466064453
		memBefore: 211196		memAfter: 211984