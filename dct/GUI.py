from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import QIntValidator
from scipy.fftpack import dct,idct
import matplotlib.pyplot as plt
from PIL import Image
import numpy

class MainWindow(QMainWindow):

    def __init__(self,*args,**kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)

        self.setWindowTitle("Trasformata DCT")

        # Layout di tipo form
        layout = QFormLayout()

        # Definizione widgets
        self.f_label = QLabel("F:")
        self.d_label = QLabel("d:")
        self.f_textbox = QLineEdit()
        self.d_textbox = QLineEdit()
        self.only_int = QIntValidator()
        self.f_textbox.setValidator(self.only_int)
        self.d_textbox.setValidator(self.only_int)
        self.select_button = QPushButton("Seleziona immagine")
        self.select_button.clicked.connect(self.selectButtonClicked)

        # Imposto la posizione degli oggetti nel layout
        layout.addRow(self.f_label,self.f_textbox)
        layout.addRow(self.d_label,self.d_textbox)
        layout.addRow(self.select_button)

        # Visualizzazione
        widget = QWidget()
        widget.setLayout(layout)
        self.setCentralWidget(widget)
    
    # Evento click sul bottone
    def selectButtonClicked(self):
        # Ottengo il path dell'immagine
        filename,_ = QFileDialog.getOpenFileName(filter="Bitmap (*.bmp)")

        if filename == "": return

        # Apro l'immagine, converto in scala di grigio e poi in array
        img = Image.open(filename).convert("L")
        grey_array = numpy.array(img)

        plt.figure(1)
        plt.hist(grey_array.flatten(),256,[0,256])
        plt.show()

        f = int(self.f_textbox.text())
        d = int(self.d_textbox.text())

        if not (d >= 0 and d <= (2 * f - 2)):
            print("Errore su parametro D")
            return
        
        # Ottengo dimensioni matrice
        rows,cols = numpy.shape(grey_array)

        # Blocchi per riga e per colonna
        blocks_in_a_row = int(rows / f)
        blocks_in_a_col = int(cols / f)

        #Blocchi totali
        tot_blocks = blocks_in_a_row * blocks_in_a_col
        
        compressed = grey_array

        #Array di debug (non necessari per l'algoritmo)
        blocks = []
        dct_blocks = []
        dct_diag = []
        idct_blocks = []
        idct_rounded = []

        #Divisione in blocchi
        for row in range(0,rows,f):
            #Controllo di non sforare con le righe
            if( row+f ) > rows:
                break
            
            for col in range(0,cols,f):
                # Controllo di non sforare con le colonne
                if (col + f) <= cols:
                    block = grey_array[row:row+f,col:col+f] # Blocco
                    blocks.append(block) # Array di blocchi
                    
                    #Applico dct sul blocco
                    dct_block = dct(dct(block,norm="ortho").T,norm="ortho").T
                    dct_blocks.append(dct_block)

                    # Metto a 0 tutti i valori sotto la diagonale d
                    for r in range(f):
                        for c in range(f):
                            if(r+c)>=d:
                                dct_block[r][c] = 0
                    dct_diag.append(dct_block)
                    
                    # Applico idct sul blocco
                    idct_block = idct(idct(dct_block,norm="ortho").T,norm="ortho").T
                    idct_blocks.append(idct_block)

                    # Arrotondo e setto i valori fuori bound
                    for r in range(f):
                        for c in range(f):
                            idct_block[r][c] = round(idct_block[r][c])
                            if idct_block[r][c] < 0: idct_block[r][c] = 0
                            elif idct_block[r][c] > 255: idct_block[r][c] = 255
                    idct_rounded.append(idct_block)

                    block_row = 0
                    block_col = 0

                    # Inserisco il blocco processato nella matrice
                    for i in range(row,row+f):
                        block_col = 0
                        for j in range(col,col+f):
                            compressed[i][j] = idct_block[block_row][block_col]
                            block_col += 1
                        block_row += 1

        img.show()
        result = Image.fromarray(compressed,"L")
        result.show()
        plt.figure(2)
        plt.hist(compressed.flatten(),256,[0,256])
        plt.show()
        

app = QApplication([])

window = MainWindow()
window.show()

# event loop
app.exec_()
