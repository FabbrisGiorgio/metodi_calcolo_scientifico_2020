from scipy.io import loadmat
import numpy as np
import scipy.linalg as sla

matrix_data = loadmat("ex15.mat")

matrix = matrix_data["Problem"]["A"][0][0]
row, col = matrix.shape

print(row)

b = np.ones((row,), dtype=int).dot(matrix)

print(b)

myL = np.linalg.cholesky(matrix)

# check_x = np.dot(A, b)
# check_x = np.dot(A,b)
check_x = sla.solve(matrix, b)

# check if the composition was done right
myLT = myL.T.conj()  # transpose matrix
Ac = np.dot(myL, myLT)  # should give the original matrix A

# y=np.dot(myL,b)
y = sla.solve_triangular(myL, b)

# x=np.dot(myL.T.conj(),y)
x = sla.solve_triangular(myLT, b)
